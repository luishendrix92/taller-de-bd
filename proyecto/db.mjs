import mongodb from "mongodb";

const host = "ds151970.mlab.com";
const user = "heroku_vkcbbplx";
const pass = "bcjkobrpgtsja01kde2bh0ft3n";

class Client {
  constructor() {
    this.client = null;
    this.db = null;
  }

  connect() {
    const client = new mongodb.MongoClient(`mongodb://${user}:${pass}@${host}:51970/heroku_vkcbbplx`);

    return new Promise((resolve, reject) => {
      client.connect((err) => {
        if (err) reject(err);

        this.client = client;
        this.db = client.db("heroku_vkcbbplx");

        resolve(this.db);
      });
    });
  }

  find(coll, query) {
    const collection = this.db.collection(coll);

    return new Promise((resolve, reject) => {
      collection.find(query).sort({ _id: -1 }).toArray(function(err, docs) {
        if (err) reject(err);

        resolve(docs);
      });
    });
  }

  findById(coll, id) {
    const collection = this.db.collection(coll);

    return new Promise((resolve, reject) => {
      collection.findOne({
        _id: new mongodb.ObjectID(id)
      }, function(err, doc) {
        if (err) reject(err);

        resolve(doc);
      });
    });
  }

  aggregate(coll, pipeline) {
    const collection = this.db.collection(coll);

    return new Promise((resolve, reject) => {
      collection.aggregate(pipeline).toArray(function(err, docs) {
        if (err) reject(err);

        resolve(docs);
      });
    });
  }

  insertOne(coll, doc) {
    const collection = this.db.collection(coll);

    return new Promise((resolve, reject) => {
      collection.insertOne(doc, function(err, inserted) {
        if (err) reject(err);

        resolve(inserted);
      });
    });
  }

  deleteOne(coll, id) {
    const collection = this.db.collection(coll);

    return new Promise((resolve, reject) => {
      collection.deleteOne({
        _id: new mongodb.ObjectID(id)
      }, function(err, deleted) {
        if (err) reject(err);

        resolve(deleted);
      });
    });
  }
}

const db = new Client();

export default db;
