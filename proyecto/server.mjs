import * as PageController from "./controllers/page.mjs";
import * as BookController from "./controllers/book.mjs";
import * as MagController from "./controllers/magazine.mjs";
import db from "./db.mjs";
import hbs from "express-handlebars"
import express from "express";
import path from "path";

const __dirname = path.resolve();
const app = express();

/**
 * Middleware
 */
app.use(express.urlencoded());
app.set("view engine", "hbs");
app.engine("hbs", hbs({
  extname: "hbs",
  defaultView: "default",
  layoutsDir: __dirname + "/views/layouts/",
  partialsDir: __dirname + "/views/partials/"
}));

/**
 * Route declarations
 */
app.get("/", PageController.index);
app.get("/books", BookController.list);
app.get("/books/add", BookController.add);
app.post("/books", BookController.create);
app.get("/books/:id/details", BookController.details);
app.post("/books/:id", BookController.remove);
app.get("/magazines", MagController.list);

/**
 * Wrap-up and Listener
 */
db.connect().then(function() {
  app.listen(3000, function() {
    console.log(`Parralib app running on port 3000!`);
  });  
}).catch(console.error);
