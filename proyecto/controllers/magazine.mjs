import db from "./../db.mjs";

export async function list(_req, res) {
  const magazines = await db.find("magazines", {});
  const genres = await db.aggregate("magazines", [
    { $group: { _id: "$genre" } },
    { $project: { _id: 0, genre: "$_id" } }
  ]);

  res.render("magazines", {
    title:"New magazine releases!",
    genres, magazines
  });
}
