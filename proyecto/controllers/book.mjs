import db from "./../db.mjs";

export async function list(_req, res) {
  const books = await db.find("books", {});
  const genres = await db.aggregate("books", [
    { $group: { _id: "$genre" } },
    { $project: { _id: 0, genre: "$_id" } }
  ]);

  res.render("books", {
    title:"New book releases!",
    genres,
    books
  });
}

export async function details({ params: { id } }, res) {
  const book = await db.findById("books", id);

  res.render("book", {
    title: book.title,
    book
  });
}

export async function add(_req, res) {
  res.render("add-book", {
    title: "Add new book"
  });
}

export async function create(req, res) {
  const inserted = await db.insertOne("books", {
    ...req.body,
    formats: []
  });

  res.redirect(`/books/${inserted.ops[0]._id}/details`);
}

export async function remove({ params: { id } }, res) {
  await db.deleteOne("books", id);

  res.redirect("/books");
}
