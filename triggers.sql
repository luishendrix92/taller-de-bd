CREATE FUNCTION evolve() RETURNS trigger AS $evolve$
  BEGIN
    -- Every 100 xp points there's a new evolution
    -- form up to the 3rd level. For example 0-100 = 1
    -- and 101-200 = 2, etc until > 300 then it's level 3.
    NEW.level := LEAST(CEIL(NEW.xp / 100), 3)
    
    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER evolution
BEFORE UPDATE OR INSERT ON captured_pokemon
  FOR EACH ROW EXECUTE PROCEDUREevolve();